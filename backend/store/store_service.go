package store

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"github.com/go-redis/redis/v8"
)

var (
	storeService = &StorageService{}
	ctx          = context.Background()
)

const CacheDuration = 6 * time.Hour

type StorageService struct {
	redisClient *redis.Client
}

func InitializeStore() *StorageService {
	redisClient := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "",
		DB:       0,
	})

	pong, err := redisClient.Ping(ctx).Result()
	if err != nil {
		panic(fmt.Sprintf("Error init Redis: %v", err))
	}

	fmt.Printf("\nRedis started successfully: pong message = {%s}", pong)
	storeService.redisClient = redisClient
	return storeService
}

type shortUrlID struct {
	ShortUrl          string
	User_ID           string
	Test1             string
	OriginalUrl       string
	CacheDurationTime time.Duration
	CreationTime      string
}

// if user id was not provided generate one on the fly : case for not logged in users

/* We want to be able to save the mapping between the originalUrl
and the generated shortUrl url
*/
func SaveUrlMapping(shortUrl string, originalUrl string, userId string, creationTime string) {
	var test1 = "Hello"

	shortUrlData := shortUrlID{
		ShortUrl:          shortUrl,
		User_ID:           userId,
		Test1:             test1,
		OriginalUrl:       originalUrl,
		CacheDurationTime: CacheDuration,
		CreationTime:      creationTime,
	}

	json, err2 := json.Marshal(shortUrlData)
	if err2 != nil {
		fmt.Println(err2)
	}
	// fmt.Println("Hello")
	// fmt.Println(json)

	err := storeService.redisClient.Set(ctx, shortUrl, json, CacheDuration).Err()
	if err != nil {
		panic(fmt.Sprintf("Failed saving key url | Error: %v - shortUrl: %s - originalUrl: %s\n", err, shortUrl, originalUrl))
	}

	fmt.Printf("Saved shortUrl: %s - originalUrl: %s\n", shortUrl, originalUrl)
}

/*
We should be able to retrieve the initial long URL once the short
is provided. This is when users will be calling the shortlink in the
url, so hat we need to do here is to retrieve the long url and
think about redirect.
*/
func RetrieveInitialUrl(shortUrl string) string {
	result, err := storeService.redisClient.Get(ctx, shortUrl).Result()
	if err != nil {
		panic(fmt.Sprintf("Failed RetrieveInitialUrl url | Error: %v - shortUrl: %s\n", err, shortUrl))
	}
	var parsedJson shortUrlID
	json.Unmarshal([]byte(result), &parsedJson)
	return parsedJson.OriginalUrl
}
